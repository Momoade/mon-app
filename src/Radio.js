import React from 'react';
import logo from './logo.svg';
import './Radio.css';
import 'bootstrap/dist/css/bootstrap.min.css';

const radio = [{question:"Suivre cette recommandation serait bon pour ma santé.",type:"range"},{question:"Suivre cette recommandation me permettrait d’être en bonne santé.",type:"range"}, {question:"Suivre cette recommandation me permettrait d’améliorer ma condition physique.",type:"range"}, {question:"Par le passé, suivre les recommandations médicales m’a été bénéfique.",type:"range"}, {question:"Pour moi, suivre cette recommandation serait Efficace",type:"range"}, {question:"Pour moi, suivre cette recommandation serait Bon",type:"range"} ];


function Radio() {
    return (
        <div >

            <form>
                <input type="radio" id="1" name="myRadio" value="1"></input>
                <label htmlFor="1"> 1</label>
                <input type="radio" id="2" name="myRadio" value="2"></input>
                <label htmlFor="2"> 2</label>
                <input type="radio" id="3" name="myRadio" value="3"></input>
                <label htmlFor="3"> 3</label>
                <input type="radio" id="4" name="myRadio" value="4"></input>
                <label htmlFor="4"> 4</label>
                <input type="radio" id="5" name="myRadio" value="5"></input>
                <label htmlFor="5"> 5</label>
            </form>

        </div>
    );
}

export default Radio;